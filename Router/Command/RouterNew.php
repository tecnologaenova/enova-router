<?php

namespace Enova\Router\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Enova\Core\Utils\Registry;

class RouterNew extends Command {

    protected function configure() {
        $this
                ->addArgument('module', InputArgument::REQUIRED, 'Name of the Module to create')
                ->addArgument('action', InputArgument::REQUIRED, 'Name of the Action to create')
                ->addArgument('name', InputArgument::REQUIRED, 'Internal Name for reference')
                ->addArgument('methods', InputArgument::REQUIRED, 'Methods for the controller')
                ->setName("router:new");
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $modulesPath = Registry::get('root.directory');
        $module = $input->getArgument('module');
        $action = $input->getArgument('action');
        $name = $input->getArgument('name');
        $methods = $input->getArgument('methods');
        try {

            echo "$module - $action - " . print_r($methods, true) . " - $name";
            $output->writeln("<info>Routing created!</info>");
        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());
        }
    }

}
