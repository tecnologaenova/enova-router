<?php

namespace Enova\Router\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Enova\Core\Utils\Registry;

class RouterClear extends Command {

    protected function configure() {
        $this
                ->setName("router:cache:clear");
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $modulesPath = Registry::get('root.directory');
        try {
            $storage = $modulesPath . "/storage";
            shell_exec("ls -ad $storage/routes/* | grep -v '.gitignore' | xargs rm -rf");
            $output->writeln("<info>Router Cache Cleared!</info>");
        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());
        }
    }

}
