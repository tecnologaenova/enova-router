<?php

namespace Enova\Router\Implementation;

use Gears\ClassFinder;

/**
 * Router app for create cache
 */
class RouterApp {

    private $finder = null;
    private $classesPath = [];
    private $routerHandle=null;

    /**
     * Constructor method
     * @param ComposerAutoloader $composerInstance composer instance for looking for controller classes
     * @param array $namespaces namepsaces that we need to scan inside composer
     */
    public function __construct($composerInstance, array $namespaces = ['Modules']) {
        $this->finder = new ClassFinder($composerInstance);
        $this->findControllersFromNamespaces($namespaces);
        $this->routerHandle=$this->createCache();
    }

    /**
     * Method to call the class that create the routers' cache
     * @return \Enova\Router\Implementation\Router Router instance 
     */
    public function createCache() {
        return new Router($this->classesPath, // Path to controller files, will be scanned recursively
                APP_ROOT . '/storage/routes/' // Path to annotations router cache files, must be writeable by web server, if it doesn't exist, router will attempt to create it
        );
    }

    /**
     * method to find for namespaces inside composer's instance
     * @param array $namespaces array of namespaces
     */
    private function findControllersFromNamespaces(array $namespaces) {
        $classesPath = [];
        foreach ($namespaces as $namespace) {
            $classes = $this->finder->namespace($namespace)->extends(Controller::class)->search();
            $classesPath = array_merge($classesPath, $classes);
        }
        $this->classesPath =$classesPath;
    }

}
