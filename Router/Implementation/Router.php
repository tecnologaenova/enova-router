<?php

namespace Enova\Router\Implementation;

/**
 * Class Router for scan annotations and create cache files for routing in slimphp
 */
class Router {

    const ROUTE_INDEX = 0;

    private $_routesFile = null;
    private $_controllersFile = null;
    private $_usedControllers = [];
    private $_controllerDirs = null;
    private $_cacheDir = null;

    /**
     * Constructor method
     * @param string|array $controllerNamespaces namespaces previosly scanned where our controllers are 
     * @param string $cacheDir directory where we are going to put the cache files
     * @param string $routerNameFile name of the router file for the cache
     * @param string $controllerRegisterFile name of the controller register file for the cache
     * @throws \RuntimeException Exception that ocurrs when the cache is not writable
     */
    public function __construct($controllerNamespaces, $cacheDir, $routerNameFile = 'routes.php', $controllerRegisterFile = 'controllers.php') {

        $this->_routesFile = $routerNameFile;
        $this->_controllersFile = $controllerRegisterFile;

        // We save controller dirs
        if (is_string($controllerNamespaces)) {
            $controllerNamespaces = [$controllerNamespaces];
        }

        $this->_controllerDirs = $controllerNamespaces;

        // We save the cache dir
        if (!is_dir($cacheDir)) {
            $result = @mkdir($cacheDir, 0777, true);
            if ($result === false) {
                throw new \RuntimeException('Can\'t create cache directory');
            }
        }

        if (!is_writable($cacheDir)) {
            throw new \RuntimeException('Cache directory must be writable by web server');
        }

        $this->_cacheDir = rtrim($cacheDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        $this->_generateRoutes();
    }

    /**
     * Method that generate the cache files for routing
     */
    private function _generateRoutes() {
        $parsingNeeded = !file_exists($this->_cacheDir . $this->_routesFile);

        // We look for controller files
        $bundleFiles = $this->_findControllerFiles();

        // We check if there has been modifications since last cache generation
        if (!$parsingNeeded) {
            $routesCacheMtime = filemtime($this->_cacheDir . $this->_routesFile);
            foreach ($bundleFiles as $index => $bundle) {
                if ($bundle['lifetime'] > $routesCacheMtime) {
                    $parsingNeeded = true;
                    break;
                }
            }
        }

        // We look for deleted controller files
        if (!$parsingNeeded && file_exists($this->_cacheDir . $this->_controllersFile)) {
            require_once $this->_cacheDir . $this->_controllersFile;
            foreach ($this->_usedControllers as $controllerFile) {
                if (!file_exists($controllerFile)) {
                    $parsingNeeded = true;
                    break;
                }
            }
        }

        // We regenerate cache file if needed
        if ($parsingNeeded) {
            $controllerFiles = [];
            $controllerNamespaces = [];
            $commonFileContent = '<?php' . "\r\n" . '/**' . "\r\n" . ' * Slim annotations router %s cache file, self generated on ' . date('c') . "\r\n" . ' */' . "\r\n\r\n";
            $routesFileContent = sprintf($commonFileContent, 'routes');
            $controllersFileContent = sprintf($commonFileContent, 'controllers');
            foreach ($bundleFiles as $index => $bundle) {
                // We generate routes for current file
                $content = $this->_parseController($bundle['namespace']);
                if ($content !== '') {
                    $routesFileContent .= $content;
                    $controllerFiles[] = $bundle['file'];
                    $controllerNamespaces[] = $bundle['namespace'];
                }
            }

            file_put_contents($this->_cacheDir . $this->_routesFile, $routesFileContent);

            $usedControllers = (count($controllerFiles) > 0) ? '$this->_usedControllerFiles = [\'' . join('\',\'', $controllerFiles) . '\'];' : '';
            file_put_contents($this->_cacheDir . $this->_controllersFile, $controllersFileContent . $usedControllers);
        }
    }

    /**
     * Method for scan controller dirs and determine their file age
     * @return array array with the file, lifetime and namespace associated with him
     */
    private function _findControllerFiles() {
        $result = [];
        foreach ($this->_controllerDirs as $file => $namespace) {
            $result[] = [
                'file' => $file,
                'lifetime' => filemtime($file),
                'namespace' => $namespace
            ];
        }
        return $result;
    }

    /**
     * Method that scans a class and extract the annotations for get the possible routes.
     * @param string $class class to scan for get possible routes
     * @return string string created that represents the routes for the class
     */
    private function _parseController($class) {
        $fooReflection = new \Nette\Reflection\ClassType($class);
        $annotationsClass = $fooReflection->getAnnotations();

        $result = '';
        $groups = false;
        if (array_key_exists('BaseRoute', $annotationsClass)) {
            $groups = true;
            $result .= '$app->group(\'' . $annotationsClass['BaseRoute'][Router::ROUTE_INDEX] . '\', function () use ($app){';
        }
        foreach ($fooReflection->getMethods() as $method) {
            $methodAnnotations = $fooReflection->getMethod($method->name)->getAnnotations();

            if (array_key_exists('Route', $methodAnnotations)) {
                $routeAnnotations = $methodAnnotations['Route'][Router::ROUTE_INDEX];
                $uri = $routeAnnotations[Router::ROUTE_INDEX];
                $methods = explode(',', strtoupper($routeAnnotations['methods']));
                $result .= '$app->map([\'' . implode('\',\'', $methods) . '\'], \'' . $uri . '\',\'' . $class . ':' . $method->name . '\')';
                if (array_key_exists('Middleware', $methodAnnotations)) {
                    $result .= $this->generateMiddlewareCode($methodAnnotations['Middleware']);
                }
                $result .= ';' . "\r\n";
            }
        }
        if ($groups) {
            $result .= '})';
            if (array_key_exists('Middleware', $annotationsClass)) {
                $result .= $this->generateMiddlewareCode($annotationsClass['Middleware']);
            }
            $result .= ';';
        }

        return $result;
    }

    /**
     * Gerate code for Middlewares
     * @param array $middlewares array of different middlewares that applies
     * @return string generated code
     */
    private function generateMiddlewareCode($middlewares) {
        $result = '';
        foreach ($middlewares as $middleware) {
            $result .= '->add(\'' . $middleware . '\')';
        }
        return $result;
    }
    

}
