<?php

namespace Enova\Router\Implementation;
use Psr\Container\ContainerInterface;

/**
 * Base class for controllers
 */
class Controller {

    protected $container=null;
    
    /**
     * Constructor method
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

}
